pipeline {
      agent {
        label 'build-client'
    }

    environment {
      image = ""
      hubimage = ""
      dockerhubrepo = "premkritin"
      imageName = "hello-world"
      mainVersion = '11.0.0'
    }

    stages {
      stage('Assumptions') {
        steps {
          script {
            sh '''
              echo README.md
            '''
          }
        }
      }
      stage('host Local Registry') {
        steps {
          script {
            sh 'docker run -d -p 5000:5000 --restart=always --name registryserver registry:2'
          }
        }
      }
      stage('Clone Repo') {
        steps {
          checkout scm
        }
      }

      stage('Build') {
        steps {
          dir('hello-world') {
            sh 'mvn clean package'
          } 
        }
      }

      stage('Build Image locally') {
        steps {
          script {
            dir('hello-world') {
              docker.withRegistry('https://localhost:5000') {
                image = docker.build("${imageName}")
                hubimage = docker.build("${dockerhubrepo}/${imageName}")
              }
            }
          }
        }
      }

      stage('Tag Image locally') {
        steps {
          script {
            dir('hello-world') {
              docker.withRegistry('https://localhost:5000') {
                image.tag("${imageName}-latest")
              }
            }
          }
        }
      } 

      stage('Push Image locally') {
        steps {
          script {
            dir('hello-world') {
              docker.withRegistry('https://localhost:5000') {
                image.push("${mainVersion}.${env.BUILD_NUMBER}")
                image.push("${imageName}-latest")
                image.push('latest')
              }
            }
          }
        }
      }

      stage('Tag Image DockerHub') {
        steps {
          script {
            dir('hello-world') {
              docker.withRegistry('https://registry.hub.docker.com/', 'docker-hub') {
                hubimage.tag("${mainVersion}.${env.BUILD_NUMBER}")
                hubimage.tag("${imageName}-latest")
                hubimage.push("${mainVersion}.${env.BUILD_NUMBER}")
                hubimage.push("${imageName}-latest")
                hubimage.push('latest')
              }
            }
          }
        }
      }
  
 

      stage('Create EKS') {
        steps {
          script {
            sh '''
              cd ${WORKSPACE}
              chmod +x deployment.sh
              ./deployment.sh
            '''
          }
        }
      }
      stage('Deploy') {
        steps {
          script {
              sh '''
              script {
              echo "Making sure Cluster is functioning"
              aws eks list-clusters --region=eu-west-2
              echo "Fetching Credentials"
              aws eks --region eu-west-2 update-kubeconfig --name kube-cluster-01
              aws eks get-token --cluster-name kube-cluster-01 --region=eu-west-2
              echo "Create Namespace if not present"
              kubectl create ns helloworld --dry-run=client -o yaml|kubectl apply -f -
              echo "Create/Update Deployment"
              kubectl create deployment -n helloworld helloworld --image="${dockerhubrepo}"/"${imageName}":"${mainVersion}"."${BUILD_NUMBER}" --dry-run -o yaml|kubectl apply -f - 
            '''
          }
        }
      }
  }
}

