
 output  "eks-cluster-sg" {
   value =  aws_security_group.eks-cluster-sg.id
  
 }

output "vpc_security_group_ids" {
  description = "The id of the newly created vNet"
  value       = aws_security_group.rds-service.id
}
