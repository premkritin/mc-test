provider "aws" {
  region  = "eu-west-2"
  profile = "default"
  #version = "~> 2.52"

}

terraform {
  #required_version = "~> 0.13.0"
  backend "s3" {
    region  = "eu-west-2"
    profile = "default"
    key     = "eks/terraform.tfstate-01.json"
    bucket  = "iplato-terraform-backup"

  }
}

