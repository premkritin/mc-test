# Deploy into Kubernetes
namespace=helloworld
deploymentname=helloworld
kubectl cluster-info
if [ $? -gt 0 ]; then
    echo "Problem in connecting to cluster"
    exit 1;
else
   deployed=`kubectl get deploy -n $namespace $deploymentname |wc -l`
    if [ $deployed -eq 0 ]; then
        kubectl create deployment $deploymentname -n $namespace --image=

