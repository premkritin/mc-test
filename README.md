Simple hello world Application EKS using Jenkins pipeline deployment

This is a quick guide to install eks cluster and deploy hello world application on Kubernetes 2 node cluster setup with managed node.

Assumptions:
•	Jenkin VM running in Linux with required network access to eks cluster
•	Jenkins nodes need to have IAM privileges to query the permissions listed in https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html
•	Jenkins build/node need to have the following binaries
•	- aws cli
•	- aws-iam-authenticator
•	- kubectl cli
•	- eksctl cli
•	- jq

Build and configure minikube(steps): 
•	Install Docker and start the docker before proceeding with 
Docker image tag and push.
•	Create Docker private registry to store the docker images
•	Build the eks cluster with 2 node setup with auto scale enabled
•	Build Docker image for hello world application using Docker file.
•	tag the image with latest or build number  and push the image to local repo
•	push the same image to docker hub
•	build the deployment using the image in to EKS cluster





